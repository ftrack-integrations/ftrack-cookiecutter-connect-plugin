##################################
ftrack cookiecutter connect plugin
##################################

Cookiecutter template for a ftrack Connect plugin.

*****
Usage
*****

First make sure you have cookiecutter installed::

    pip install cookiecutter

Then generate your project from this template (in your current directory)::

    cookiecutter https://bitbucket.org/ftrack-integrations/ftrack-cookiecutter-connect-plugin.git

Once the project is create you should initialise a local git repo with::

    git init

Check the output - you may need to fix documentation underlining, add
dependencies etc.

*********************
Copyright and license
*********************

Copyright (c) 2014 ftrack

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this work except in compliance with the License. You may obtain a copy of the
License in the LICENSE.txt file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

